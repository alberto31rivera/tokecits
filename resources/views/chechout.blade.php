<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <meta name="description" content="Pagar Pedido. Outfit mujer, vestidos para xv, outfit para damas.">
    <meta name="keywords" content="Pagar Pedido, Outfit mujeres, blusas para compromisos, vestidos para xv, outfit damas, outfit para mujeres, vestidos para fiestas"/>
    <meta name="author" content="Tokecitos.com" />
    <meta name="copyright" content="Tokecitos.com" />
    <meta property="og:site_name" content="Tokecitos.com" />
    <meta name="robots" content="noindex"/>
    <meta name="robots" content="follow"/>
    <meta http-equiv="expires" content="43200"/>

    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Pagar Pedido</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/checkout/">

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/checkout/form-validation.css" rel="stylesheet">


    <style>
        .custom-input-file {
            background-color: #941B80;
            color: #fff;
            cursor: pointer;
            font-size: 12px;
            font-weight: bold;
            margin: 0 auto 0;
            min-height: 15px;
            overflow: hidden;
            padding: 10px;
            position: relative;
            text-align: center;
            width: 400px;
            margin-top: 20px;
        }

        .custom-input-file .input-file {
            border: 10000px solid transparent;
            cursor: pointer;
            font-size: 10000px;
            margin: 0;
            opacity: 0;
            outline: 0 none;
            padding: 0;
            position: absolute;
            right: -1000px;
            top: -1000px;
        }

    </style>
</head>

<body style="background-color: #979797;">

    <div class="container" style="background-color:#eaeeea;">
        <div class="row">
            <div class="col-md-4 mt-2">
                <img class="d-block" src="https://tokecitos.com/public/logo-dia.png" alt="tokecitos" width="100%"
                    height="65">
            </div>
            <div class="col-md-4 offset-md-4 mt-5">
                <a href="/" class="btn btn-info btn-lg btn-block" type="submit">Regresar a la Tienda</a>
            </div>
        </div>

        <div class="py-5">
            @if (!isset(Auth::user()->name))
                <a href="/login" class="btn btn-danger btn-lg btn-block text-white" type="submit">¿Eres Cliente?
                    Inicia Sesión</a>
            @else
                <div class="alert alert-primary" role="alert">
                    Usted inició sesión como: {{ Auth::user()->name }}
                </div>
            @endif

        </div>

        <div class="row">


            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">Tu Pedido</span>
                    <span class="badge badge-secondary badge-pill">{{ $cantidad }}</span>
                </h4>
                <ul class="list-group mb-3">
                    @foreach ($pedido->relacionpedido as $producto)
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">{{ $producto->detalle->titulo }}</h6>
                                <small class="text-muted"> {{ $producto->variacion->color_detalle->color }} ||
                                    {{ $producto->variacion->talla_detalle->talla }} ||
                                    {{ $producto->variacion->material_detalle->material }}
                                </small>
                                <small class="text-muted">
                                    Precio Unitario:<strong style="color: #EB3E32;"> {{ $producto->detalle->precio }}</strong><br>
                                    Cantidad:{{ $producto->cantidad }}
                                </small>
                            </div>
                            <span class="text-muted"> <strong> S/ {{ $producto->detalle->precio * $producto->cantidad }}</strong></span>
                        </li>
                    @endforeach


                    @if ($pedido->cupon_detalle != null)
                        <li class="list-group-item d-flex justify-content-between bg-light">
                            <div class="text-success">
                                <h6 class="my-0">{{ $pedido->cupon_detalle->codigo }}</h6>
                            </div>
                            <span class="text-success">
                                @if ($pedido->cupon_detalle->tipo == 2)
                                    -S/ {{ $pedido->cupon_detalle->aplica }}
                                @else
                                    % {{ $pedido->cupon_detalle->aplica }}
                                @endif
                            </span>
                        </li>
                    @endif


                    <li class="list-group-item d-flex justify-content-between">
                        <span>Total (Soles)</span>
                        <strong>{{ round($pedido->total_descuento) }}</strong>
                    </li>
                </ul>




            </div>

            <div class="col-md-7 order-md-1" id="direccionesBasa">
                 
                <h4 class="mb-3">Dirección</h4>
                @if (Auth::guest())
                @else
                    <div class="form-check">
                        <input class="form-check-input" @if ($direcciones == null) disabled @endif type="radio"
                            name="tipoDireccion" id="tipoDireccion1" value="1" onclick="handleClick(this);">
                        <label class="form-check-label" for="tipoDireccion1">
                            Direcciones ya registradas
                        </label>
                    </div>
                @endif

                <div class="form-check" style="margin-bottom: 2rem">
                    <input class="form-check-input" type="radio" name="tipoDireccion" id="tipoDireccion2" checked
                        value="2" onclick="handleClick(this);">
                    <label class="form-check-label" for="tipoDireccion2">
                        Registrar nueva dirección @if (Auth::guest())
                            <strong>Se creará una cuenta con el email que proporcione</strong>
                        @endif
                    </label>
                </div>

                <div id="direccionesRegistradas">
                    <h5>Seleccione una dirección</h5>

                    <ul class="list-group">
                        @if ($direcciones != null)
                            @foreach ($direcciones as $direccion)
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-md-1 col-sm-2 col-xs-2 col-2">
                                            <input class="form-check-input ml-1" type="radio" name="idDireccion"
                                                value="{{ $direccion->id }}" onclick="asignarDireccion(this);">
                                        </div>
                                        <div class="col-md-11 col-sm-10 col-xs-10 col-10">
                                            Documento: {{ $direccion->dni }} ({{ $direccion->nombre }}
                                            {{ $direccion->apellido }}) {{ $direccion->calle }} -
                                            {{ $direccion->referencia }}
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @endif
                    </ul>

                    <button class="btn btn-primary btn-lg btn-block mt-2" onclick="actualizarDireccion();"
                        type="submit">Proceder al Pago</button>
                </div>

                <div class="needs-validation" id="nuevaDireccion" novalidate>
                    <div class="mb-3">
                        <label for="address">DNI</label>
                        <input type="text" class="form-control" id="dni" placeholder="" required>
                        <div class="invalid-feedback">
                            Introduzca su número de DNI
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">Nombre</label>
                            <input type="text" class="form-control" id="nombre" placeholder="" value=""
                                required>
                            <div class="invalid-feedback">
                                Nombre es requerido
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName">Apellido</label>
                            <input type="text" class="form-control" id="apellido" placeholder="" value=""
                                required>
                            <div class="invalid-feedback">
                                Apellido es requerido
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="email">Email <span class="text-muted">*</span></label>
                            <input name="correo" type="email" class="form-control" id="email"
                                @if (isset(Auth::user()->email)) value="{{ Auth::user()->email }}" @endif required>
                            <div class="invalid-feedback">
                                El Correo es obligatorio
                            </div>
                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="email">Telefono <span class="text-muted">*</span></label>
                            <input type="text" class="form-control" id="telefono" placeholder="" required>
                            <div class="invalid-feedback">
                                Celular es obligatorio
                            </div>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="address">Dirección:</label>
                        <input type="text" class="form-control" id="direccion" placeholder="" required>
                        <div class="invalid-feedback">
                            Ingrese la dirección de envío
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="referencia"><span class="text-muted">(Opcional):</span> Referencia de su casa y/o si otra persona recibe el pedido, especificar aquí: </label>
                        <input type="text" class="form-control" id="referencia" placeholder="Detalles del pedido">
                    </div>

                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for="country">Departamento:</label>
                            <select name="departamento" id="departamento" class="custom-select d-block w-100"
                                onchange="getProvincias()">
                                <option value="">Seleccione</option>
                                @foreach ($departamentos as $departamento)
                                    <option value="{{ $departamento->id }}">
                                        {{ $departamento->name }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un Departamento.
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="state">Provincia:</label>
                            <select required name="provincia" id="provincia" class="custom-select d-block w-100"
                                onchange="getDistritos()">
                                <option value="">Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione una provincia
                            </div>
                        </div>

                        <div class="col-md-4 mb-3">
                            <label for="state">Distrito:</label>
                            <select required name="distrito" class="custom-select d-block w-100" id="distrito">
                                <option value="">Seleccione</option>
                            </select>
                            <div class="invalid-feedback">
                                Seleccione un distrito.
                            </div>
                        </div>

                    </div>
                    <hr class="mb-4">

                    <button class="btn btn-primary btn-lg btn-block" type="button"
                        onclick="registrarNuevaDireccion();">Enviar</button>
                </div>
            </div>

            <div class="col-md-8 order-md-1" id="pasarela" style="display: none">
                <h4 class="mb-3">Seleccione método de pago</h4>

                {{--  <div class="form-check">
                    <input class="form-check-input" type="radio"
                        name="tipoPago" id="tipoPago1" value="1" onclick="procederPago(this);">
                    <label class="form-check-label" for="tipoPago1">
                        Tarjeta o Yape
                    </label>
                </div>  --}}

                <div class="form-check">
                    <input class="form-check-input" type="radio"
                        name="tipoPago" id="tipoPago2" value="2" onclick="procederPago(this);">
                    <label class="form-check-label" for="tipoPago2">
                        Yape / Plin / Tranferencia
                    </label>
                </div>

                <div class="col-md-12" id="pagoYapeDiv" style="display: none">
                    <form action="{{route('pagoYape')}}" accept-charset="UTF-8" enctype="multipart/form-data" method="post">
                        @csrf
                        <input type="hidden" name="correo" id="correo_yape" @if (isset(Auth::user()->email)) value="{{ Auth::user()->email }}" @endif>
                        <input type="hidden" name="idPedido" value="{{ $pedido->id }}">
                        <div class="row mt-3">
                            <div class="col-12 py-2">
                                <img class="mx-auto d-block" src="{{ asset('img/payment/yapebeto.jpg') }}" width="70%" alt="yape">
                            </div>
                            <div class="col-12   py-2">
                                <img class="mx-auto d-block" src="{{ asset('img/payment/plinbeto.jpg') }}" width="70%" alt="plin">
                            </div>
                            <div class="col-12 py-4">
                                <p> <strong>BCP Cuenta Ahorros (Sullana)</strong>  <br>
                                N° de Cuenta Soles: 535-93544450-0-61<br>
                                Nombre: Esteban Alberto Rivera Leiva.</p>

                                <p><strong>BCP Cuenta de Ahorros: (LIMA)</strong><br>
                                N° de Cuenta Soles: 193-30154132-0-18<br>
                                Nombre: Esteban Alberto Rivera Leiva.</p>

                                <p><strong>CAJA SULLANA: 33860</strong><br>
                                Nombre: Esteban Alberto Rivera Leiva.</p>

                                <p><strong>BANCO CONTINENTAL</strong><br>
                                Cta Independencia:<br>
                                0011-0274-81-0200412517<br>
                                Nombre: Esteban Alberto Rivera Leiva.</p>

                                <p><strong>BANCO CONTINENTAL</strong><br>
                                Código de Cuenta Interbancaria (CCI)<br>
                                011-274-000200412517-81</p>

                            </div>

                            <div class="custom-input-file col-md-6 col-sm-6 col-xs-6">
                                <input type="file" id="fichero-tarifas"
                                    class="input-file" required value="" name="comprobante">
                                Subir Comprobante
                            </div>




                            <div class="container">
                                <div class="row justify-content-md-center">
                                    <div class="col col-lg-2">

                                    </div>
                                    <div class="col-md-auto">
                                    <button class="form-control btn btn-success mt-5" type="submit">Enviar</button>
                                    </div>
                                    <div class="col col-lg-2">

                                    </div>
                                </div>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">&copy; 2023 Tokecitos</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="https://tokecitos.com/politica">Política</a></li>
              <!--   <li class="list-inline-item"><a href="#">Terms</a></li> -->
                <li class="list-inline-item"><a href="https://wa.link/xozcmd" target="_blank">Soporte</a></li>
            </ul>
        </footer>
    </div>

    <input type="hidden" name="idPedido" id="idPedido" value="{{ $pedido->id }}">

    @if ($pedido->cupon_detalle != null)
        <input type="hidden" name="total" id="total" value="{{ $pedido->total_descuento }}">
    @else
        <input type="hidden" name="total" id="total" value="{{ $pedido->total }}">
    @endif
    <input type="hidden" name="correo_proceso" id="correo_proceso" @if (isset(Auth::user()->email)) value="{{ Auth::user()->email }}" @endif>



    {{--  Inicio Modal Plin / Transferencia  --}}

    <div class="modal" tabindex="-1" role="dialog" id="modalYape">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">Save changes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>






    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script>
        window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://checkout.culqi.com/js/v4"></script>
    <script>

        monto = $("#total").val() * 100
        orden = 'orden_live_'+$("#idPedido").val()

        Culqi.publicKey = 'pk_test_bs6MyrDKWn7rfMxK';

        Culqi.settings({
            title: 'Culqi Store',
            currency: 'PEN',  // Este parámetro es requerido para realizar pagos yape
            amount: monto,  // Este parámetro es requerido para realizar pagos yape
            order: orden // Este parámetro es requerido para realizar pagos con pagoEfectivo, billeteras y Cuotéalo
          });


          Culqi.options({
            lang: "auto",
            installments: false, // Habilitar o deshabilitar el campo de cuotas
            paymentMethods: {
              tarjeta: true,
              yape: true,
              bancaMovil: false,
              agente: false,
              billetera: false,
              cuotealo: false,
            },
          });

          Culqi.options({
            style: {
              logo: 'https://tokecitos.com/public/logo-dia.png',
              bannerColor: '#17a2b8', // hexadecimal
              buttonBackground: '#17a2b8', // hexadecimal
              menuColor: '', // hexadecimal
              linksColor: '#17a2b8', // hexadecimal
              buttonText: 'Realizar Pago', // texto que tomará el botón
              buttonTextColor: '', // hexadecimal
              priceColor: '#17a2b8' // hexadecimal
            }
        });


        function culqi() {
            if (Culqi.token) {  // ¡Objeto Token creado exitosamente!
              const token = Culqi.token.id;

                formData = {
                    tokenCulqi: token,
                    idPedido: $("#idPedido").val(),
                    correo: $("#correo_proceso").val(),
                    monto: $("#total").val()
                }

                axios.post('/procesarYapeTarejta', formData).then((response) => {
                    if(response.data == 200){
                        window.location.href = "/pagoExitoso"
                    }
                })

            } else if (Culqi.order) {  // ¡Objeto Order creado exitosamente!
              const order = Culqi.order;
              console.log('Se ha creado el objeto Order: ', order);

            } else {
              // Mostramos JSON de objeto error en consola
              console.log('Error : ',Culqi.error);
            }
          };



        $("#direccionesRegistradas").css("display", "none");
        $("#nuevaDireccion").css("display", "block");
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';

            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');

                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();

        function agregarCupon() {
            formData = {
                idPedido: $("#idPedido").val(),
                cupon: $("#cupon").val()
            }

            axios.post('/aplicarCupon', formData).then((response) => {
                location.reload()
            })
        }

        var currentValue = 2;
        var idDireccion = 0;
        var tipoPago = 0;

        function asignarDireccion(myRadio) {
            idDireccion = myRadio.value
            console.log(myRadio.value)
        }

        function handleClick(myRadio) {
            currentValue = myRadio.value;

            if (currentValue == 1) {
                $("#direccionesRegistradas").css("display", "block");
                $("#nuevaDireccion").css("display", "none");
            } else {
                $("#direccionesRegistradas").css("display", "none");
                $("#nuevaDireccion").css("display", "block");
            }
        }

        function procederPago(myRadio) {
            if(myRadio.value == 1){
                Culqi.open();
            }
            if(myRadio.value == 2){
                $("#pagoYapeDiv").css('display', 'block')
            }
        }

        $(document).ready(function() {
            $('#departamento').select2();
            $("#provincia").select2();
            $("#distrito").select2()
        });

        function getProvincias() {
            axios.get('/provincias/' + $("#departamento").val()).then((response) => {
                var provincias = $("#provincia");
                provincias.html(response.data.html)
            })
        }

        function getDistritos() {
            axios.get('/distritos/' + $("#provincia").val()).then((response) => {
                var distrito = $("#distrito");
                distrito.html(response.data.html)
            })
        }

        function actualizarDireccion() {
            formData = {
                idPedido: $("#idPedido").val(),
                direccion: idDireccion
            }

            axios.post('/actualizarDireccionPedido', formData).then((response) => {
                if (response.data == 200) {
                    $("#direccionesBasa").css("display", "none");
                    $("#pasarela").css("display", "block");
                }
            })
        }

        function registrarNuevaDireccion() {
            formData = {
                nombre: $("#nombre").val(),
                apellido: $("#apellido").val(),
                departamento: $("#departamento").val(),
                provincia: $("#provincia").val(),
                distrito: $("#distrito").val(),
                direccion: $("#direccion").val(),
                referencia: $("#referencia").val(),
                telefono: $("#telefono").val(),
                correo: $("#email").val(),
                idPedido: $("#idPedido").val(),
                dni: $("#dni").val()
            }

            axios.post('/registrarNuevaDireccion', formData).then((response) => {
                idDireccion = response.data
                console.log(idDireccion)
                $("#correo_proceso").val($("#email").val())
                $("#correo_yape").val($("#email").val())
                $("#direccionesBasa").css("display", "none");
                $("#pasarela").css("display", "block");
            })
        }
    </script>
</body>

</html>
