
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-102181603-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-102181603-1');
</script>

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '5865780173500554');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=5865780173500554&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->


<link rel="shortcut icon" type="image/x-icon" href="{{ route('welcome') }}/public/img/favicon.ico">

    <!-- Ionicons Font CSS-->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <!-- font awesome CSS-->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Animate CSS-->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- UI CSS-->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <!-- Chosen CSS-->
    <link rel="stylesheet" href="{{ asset('css/chosen.css') }}">
    <!-- Meanmenu CSS-->
    <link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">
    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
    <!-- Normalize CSS-->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- Nivo Slider CSS-->
    <link rel="stylesheet" href="{{ asset('css/nivo-slider.css') }}">
    <!-- Owl Carousel CSS-->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <!-- EasyZoom CSS-->
    <link rel="stylesheet" href="{{ asset('css/easyzoom.css') }}">
    <!-- Slick CSS-->
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ env('URL_default') }}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ env('URL_STYLES') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.0/flexslider.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
<script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
