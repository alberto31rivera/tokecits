<!doctype html>
<html class="no-js" lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Inicio - Tokecitos.com Tienda Online Outfit para mujeres.</title>
    <meta name="description" content="Tokecitos - Tienda Online - Venta en línea. Outfit de mujeres. Vestidos de XV.">
    <meta name="keywords" content="tokecitos, outfit mujeres, outfit para mujeres, vestidos noche, vestidos para fiestas"/>
    <meta name="author" content="Tokecitos.com" />
    <meta name="copyright" content="Tokecitos.com" />
    <meta property="og:site_name" content="Tokecitos.com" />
    <meta name="robots" content="index"/>
    <meta name="robots" content="follow"/>
    <meta http-equiv="expires" content="43200"/>
    <meta property=”og:image” content=”https://tokecitos.com/img/slider/3-min.jpg” />


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.rels')
</head>

<body>
    <!--[if lt IE 8]>
 <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->
        <!--Slider Area Start-->
        <section class="slider-area mb-30">
            <div class="slider-wrapper theme-default">
                <!--Slider Background Image Start-->
                <div id="slider" class="nivoSlider">
                    <img src="img/slider/3-min.jpg" alt="verano temporada verano putfit tokecitos" title="#htmlcaption" />
                    <img src="img/slider/4-min.jpg" alt="verano temporada verano putfit tokecitos" title="#htmlcaption2" />
                    <img src="img/slider/3-min.jpg" alt="verano temporada verano putfit tokecitos" title="#htmlcaption3" />
                </div>
                <!--Slider Background Image End-->
                <!--1st Slider Caption Start-->
                <div id="htmlcaption" class="nivo-html-caption">
                    <div class="slider-caption">
                        <div class="slider-text">
                            <h5 class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">Oferta
                                Exclusiva Delivery Gratis   </h5>
                            <h1 class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                              Colección
                                <br><span>Outfit mujer</span></h1>
                            <h4 class="wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">Precios
                                desde <span>S/ 39.00</span></h4>
                            <div class="slider-button">
                                <a href="https://tokecitos.com/tienda" class="wow button animated fadeInLeft" data-text="Shop now"
                                    data-wow-duration="2.5s" data-wow-delay="0.5s">Comprar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--1st Slider Caption End-->
                <!--2nd Slider Caption Start-->
                <div id="htmlcaption2" class="nivo-html-caption">
                    <div class="slider-caption">
                        <div class="slider-text">
                            <h5 class="wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
                            Oferta Exclusiva Delivery Gratis</h5>
                            <h1 class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">Ideal para <br><span>fiestas</span></h1>
                            <h4 class="wow animated fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s">desde
                                 <span>S/ 59</span></h4>
                            <div class="slider-button">
                                <a href="https://tokecitos.com/tienda" class="wow button animated fadeInLeft" data-text="Shop now"
                                    data-wow-duration="2.5s" data-wow-delay="0.5s">Ver diseños</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--2nd Slider Caption End-->
                <!--3rd Slider Caption Start-->
                <div id="htmlcaption3" class="nivo-html-caption">
                    <div class="slider-caption">
                        <div class="slider-text">
                            <h5 class="wow animated fadeInLeft" data-wow-duration="1.5s" data-wow-delay="0.5s">
                                Temporada 2023 en Tokecitos </h5>
                            <h1 class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">Diseños Exclusivos<br><span>para ti </span></h1>
                            <h4 class="wow animated fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s">desde
                                 <span>S/ 59</span></h4>
                            <div class="slider-button">
                                <a href="https://tokecitos.com/tienda" class="wow button animated fadeInLeft" data-text="Shop now"
                                    data-wow-duration="2.5s" data-wow-delay="0.5s">Ver diseños</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--3rd Slider Caption End-->
            </div>
        </section>



        <!-- <div class="offer-area mb-50">
            <div class="container">
                <div class="row">

                    <div class="col-lg-4 col-md-4">
                        <div class="single-offer">
                            <div class="offer-img img-full">
                                <a href="{{ route('welcome') }}/video">
                                    <img src="{{ route('welcome') }}/public/img/publicidad/video-youtube1-min.jpg" alt="video top semanal">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="single-offer">
                            <div class="offer-img img-full">
                                <a href="{{ route('welcome') }}/video">
                                    <img src="{{ route('welcome') }}/public/img/publicidad/video-youtube2-min.jpg" alt="video top semanal">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="single-offer">
                            <div class="offer-img img-full">
                                <a href="{{ route('welcome') }}/video">
                                    <img src="{{ route('welcome') }}/public/img/publicidad/video-youtube3-min.jpg" alt="video top semanal">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div> -->


        <section class="hot-deal-product-of-the-day mb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--Section Title1 Start-->
                        <div class="section-title1-border">
                            <div class="section-title1">
                                <h3>Llévalo hoy</h3>
                            </div>
                        </div>
                        <!--Section Title1 End-->
                    </div>
                </div>
                <div class="row">
                    <div class="hot-deal-of-product owl-carousel">
                        <!--Single Product Start-->
                        <div class="col-lg-12">
                            <div class="single-product hot-deal-list">
                                <div class="product-img">
                                    <a href="https://tokecitos.com/outfit/bf4023f8-ed60-42f9-8b2d-891035655d5c">
                                        <img class="first-img" src="https://tokecitos.com/uploads/productimage/63c4cfe24dfaf_1673842658.jpg" alt="producto {{ config('app.name') }} llevalo hoy oferta">
                                        <img class="hover-img" src="https://tokecitos.com/uploads/productimage/63c4cfe24ef4a_1673842658.jpg" alt="producto {{ config('app.name') }} llevalo hoy oferta">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="pro-title"><a href="https://tokecitos.com/outfit/bf4023f8-ed60-42f9-8b2d-891035655d5c">Vestido Tatiana Camay. Outfit De Mujer.</a></h2>
                                    <div class="pro-rating-price">
                                        <div class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div class="product-price">
                                            <span class="old-price">S/65.00</span>
                                            <span class="new-price">S/59.00</span>
                                        </div>
                                        <div class="hot-deal-product-des">
                                            <p>Modelo exclusivo de nuestra tienda, llévalo hoy no dejes pasar esta oportunidad.</p>
                                        </div>
                                        <div class="count-down-box">
                                            <div class="count-box">
                                                <div class="pro-countdown" data-countdown="2023/01/31"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Single Product End-->
                        <!--Single Product Start-->
                        <div class="col-lg-12">
                            <div class="single-product hot-deal-list">
                                <div class="product-img">
                                    <a href="https://tokecitos.com/outfit/7017286b-ad35-45ee-9d33-a76382d3d3c0">
                                        <img class="first-img" src="https://tokecitos.com/uploads/productimage/63c4c5df6d0ae_1673840095.jpg" alt="producto {{ config('app.name') }} llevalo hoy oferta">
                                        <img class="hover-img" src="https://tokecitos.com/uploads/productimage/63c4c5df6d5ea_1673840095.jpg" alt="producto {{ config('app.name') }} llevalo hoy oferta">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <h2 class="pro-title"><a href="https://tokecitos.com/outfit/7017286b-ad35-45ee-9d33-a76382d3d3c0">Blusa Mary Bobo Cruzado - Outfit Mujer.</a></h2>
                                    <div class="pro-rating-price">
                                        <div class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div class="product-price">
                                            <span class="old-price">S/54.00</span>
                                            <span class="new-price">S/49.00</span>
                                        </div>
                                        <div class="hot-deal-product-des">
                                            <p>Se convertirá en tu blusa preferida. El mejor outfit de mujer lo puedes tener en Tokecitos. </p>
                                        </div>
                                        <div class="count-down-box">
                                            <div class="count-box">
                                                <div class="pro-countdown" data-countdown="2023/01/31"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Single Product End-->
                        <!--Single Product Start-->


                    </div>
                </div>
            </div>
        </section>


        <!--Offer Image Area End-->
        <!--New Arrival Product Start-->
        <section class="new-arrival-product mb-20">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--Section Title1 Start-->
                        <div class="section-title1-border">
                            <div class="section-title1">
                                <h3> <a href="https://tokecitos.com/tienda">Ver Tienda </a> </h3>
                            </div>
                        </div>
                        <!--Section Title1 End-->
                    </div>
                </div>

                <!--Product Tab Start-->
                <div class="tab-content">
                    <div id="laptop" class="tab-pane fade show active">
                        <div class="row">
                            <div class="new-arrival-list-product owl-carousel">

                                <!--Single Product Start-->
                                @if($productos != null)
                                @foreach ($productos as $producto)
                                    <div class="col-lg-12">
                                        <div class="row no-gutters single-product style-2">
                                            <div class="col-4">
                                                <div class="product-img">
                                                    <a href="/outfit/{{ $producto->uuid }}">
                                                        <img class="first-img" src="{{ route('welcome') }}/uploads/productimage/{{$producto->imagenes->imagen}}" alt="{{$producto->titulo}}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="product-content">
                                                    <h2 style="font-weight: 500!important;color:#040404;"><a href="/outfit/{{ $producto->uuid }}">
                                                    {{ Str::limit($producto->titulo, 60) }} 
                                                    </a></h2>

                                                    <div class="product-price">
                                                        <span class="new-price">S/ {{$producto->precio}}</span>
                                                    </div>

                                                    <p>{{ Str::limit($producto->descripcion, 80) }}</p>
                                                    <!-- <span class="badge badge-light">Mujer</span> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                                <!--Single Product End-->

                            

                            </div>
                        </div>
                    </div>

                </div>

                


            </div>
        </section>
        <!--New Arrival Product End-->

        <section class="corporate-about white-bg pb-30">
            <div class="container">
                <div class="row all-about no-gutters">
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="single-about">
                            <div class="block-wrapper">
                                <div class="about-content">
                                    <h5 style="font-size: 13px;">Diseños Exclusivos</h5>
                                    <p>Siempre a la moda</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="single-about">
                            <div class="block-wrapper2">
                                <div class="about-content">
                                    <h5 style="font-size: 13px;">100% algodón </h5>
                                    <p>Materiales de Calidad </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="single-about">
                            <div class="block-wrapper3">
                                <div class="about-content">
                                    <h5 style="font-size: 13px;">Precios Bajos</h5>
                                    <p>Y los mejores descuentos</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-6">
                        <div class="single-about not-border">
                            <div class="block-wrapper4">
                                <div class="about-content">
                                    <h5 style="font-size: 13px;">Compra segura</h5>
                                    <p>Y todos los medios</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <Section class="latest-posts-blog-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--Section Title1 Start-->
                        <div class="section-title1-border">
                            <div class="section-title1">
                                <h3>Próximamente</h3>
                            </div>
                        </div>
                        <!--Section Title1 End-->
                    </div>
                </div>
                <!--Latest Blog Start-->
                <div class="latest-blog">
                    <div class="row">
                        <div class="col-lg-12 pb-3">
                            <div class="latest-blog-active owl-carousel">
                                <!--Single Latest Blog Start-->
                                <div class="our-single-blog">
                                    <div class="our-blog-img img-full">
                                        <a href="#">
                                            <img src="http://tokecitos.com/public/img/blog/tendencias1.jpg" alt="Principales tendencias Europa de Ropa">

                                            <!-- {{ route('welcome') }} -->
                                        </a>
                                    </div>
                                    <div class="our-blog-content mt-15">
                                        <h3><a href="#">Principales tendencias europeas! </a></h3>
                       <!--                  <span class="our-blog-author">por <a href="blog-author.html">{{ config('app.name') }}</a></span> -->
                                        <div class="our-blog-des mb-20">
                                            <p>Nuestras prendas mantienen las tendencias europeas de la moda.</p>
                                        </div>
                                        {{-- <div class="blog-meta">
                                            <span class="post-comment"><i class="ion-clipboard"></i>111 comentarios</span>
                                            <span class="blog-post-date pull-right">24/05/2022</span>
                                        </div> --}}
                                    </div>
                                </div>


                                <div class="our-single-blog">
                                    <div class="our-blog-img img-full">
                                        <a href="#">
                                            <img src="http://tokecitos.com/public/img/blog/tendencias2.jpg" alt="Tips para compromisos exclusivos">
                                        </a>
                                    </div>
                                    <div class="our-blog-content mt-15">
                                        <h3><a href="#"> Tips para compromisos exclusivos</a></h3>
                                       <!--  <span class="our-blog-author">By <a href="blog-author.html">{{ config('app.name') }}</a></span> -->
                                        <div class="our-blog-des mb-20">
                                            <p>Nuestra ropa te ayudará a ser la más mirada en tus compromisos.</p>
                                        </div>
                                        {{-- <div class="blog-meta">
                                            <span class="post-comment"><i class="ion-clipboard"></i>3 comentarios</span>
                                            <span class="blog-post-date pull-right">23/05/2022</span>
                                        </div> --}}
                                    </div>
                                </div>

                                 



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </Section>



        <!--Footer Area Start-->
        @include('layouts.footer')
         
    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="js/vendor/modernizr-3.6.0.min.js"></script>
    <script src="js/vendor/jquery-3.6.0.min.js"></script>
    <script src="js/vendor/jquery-migrate-3.3.2.min.js"></script>
    <!--Popper-->
    <script src="js/popper.min.js"></script>
    <!--Bootstrap-->
    <script src="js/bootstrap.min.js"></script>
    <!--Imagesloaded-->
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <!--Isotope-->
    <script src="js/isotope.pkgd.min.js"></script>
    <!--Ui js-->
    <script src="js/jquery-ui.min.js"></script>
    <!--Countdown-->
    <script src="js/jquery.countdown.min.js"></script>
    <!--Counterup-->
    <script src="js/jquery.counterup.min.js"></script>
    <!--ScrollUp-->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!--Chosen js-->
    <script src="js/chosen.jquery.js"></script>
    <!--Meanmenu js-->
    <script src="js/jquery.meanmenu.min.js"></script>
    <!--Instafeed-->
    <script src="js/instafeed.min.js"></script>
    <!--EasyZoom-->
    <script src="js/easyzoom.min.js"></script>
    <!--Fancybox-->
    <script src="js/jquery.fancybox.pack.js"></script>
    <!--Nivo Slider-->
    <script src="js/jquery.nivo.slider.js"></script>
    <!--Waypoints-->
    <script src="js/waypoints.min.js"></script>
    <!--Carousel-->
    <script src="js/owl.carousel.min.js"></script>
    <!--Slick-->
    <script src="js/slick.min.js"></script>
    <!--Wow-->
    <script src="js/wow.min.js"></script>
    <!--Plugins-->
    <script src="js/plugins.js"></script>
    <!--Main Js-->
    <script src="js/main.js"></script>
</body>

</html>
