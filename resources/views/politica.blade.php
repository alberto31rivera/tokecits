<?php $total_pedido = 0; ?>
<!doctype html>
<html class="no-js" lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Política</title>
    <meta name="description" content="Política de envíos y devoluciones. Outfit mujer, vestidos para xv, outfit para damas.">
    <meta name="keywords" content="outfit mujeres, blusas para compromisos, vestidos para xv, outfit damas, outfit para mujeres, vestidos para fiestas"/>
    <meta name="author" content="Tokecitos.com" />
    <meta name="copyright" content="Tokecitos.com" />
    <meta property="og:site_name" content="Tokecitos.com" />
    <meta name="robots" content="index"/>
    <meta name="robots" content="follow"/>
    <meta http-equiv="expires" content="43200"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.rels')

</head>

<body>

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->

        <section class="heading-banner-area pt-30">
		    <div class="container">
		        <div class="row">
		            <div class="col-lg-12">
		                <div class="heading-banner">
		                    <div class="breadcrumbs">
		                        <ul>
		                            <li><a href="https://tokecitos.com">Inicio</a><span class="breadcome-separator">></span></li>
		                            <li>POLÍTICA</li>
		                        </ul>
		                    </div>
		                    <div class="heading-banner-title">
		                        <h1>POLÍTICA DE LA TIENDA, ENVÍO Y DEVOLUCIONES</h1>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>


        <section class="about-us-area">

            <div class="about-us-img bg-4"></div>

		    <div class="container-fluid">
		        <div class="row">

                    <div class="col-lg-6 offset-lg-6 col-md-6 offset-md-6 about-us-content">
                        <div class="about-us-title text-center">
                            <h2><strong>POLÍTICAS </strong></h2>
                        </div>
                      
<p>Tenemos por política los siguientes ítems:</p> <br>

<p>1.- Los envíos se realizan de acuerdo a la fecha en que se REALIZÓ la compra, siempre y cuando se hayan APROBADO. Para saber si su compra se aprobó debe ingresar en su panel y dirigirse a Mi Cuenta -> Mis pedidos y revisar el estado del mismo. 
   <br> En caso de no aprobarse o incurrir en cualquier inconveniente, el despacho se realizará de manera programa y si hay demora se pasará al siguiente turno de entrega.</p> <br>

<p>2.- De los pagos por Yape/Plin/Tarjeta de crédito: en caso de haber inconvenientes con estos pagos, se realizado una compra por error, note que han hecho compras no autorizadas u otras similares; puede solicitar una devolución en un plazo de 24 horas. Y ésta devolución se hará de manera automática en su aplicativo o tarjeta de crédito. Pasadas las 24 horas, entrará en un proceso de devolución manual pudiendo utilizar otros medios para transferirle siempre y cuando determinemos que realmente amerita la devolución. </p> <br>

<p>3.- La hora de reparto de los pedidos cuando es a delivery, es la siguiente:</p> <br>
 
<p>Turno 1: Si la compra se realizó hasta las 8 am (y/o 8 horas antes): el delivery reparte a partir de las 10 am hasta la 1 pm.</p> 
<p>Turno 2: Si la compra se realizó desde las 8 am hasta la 1 pm: el delivery reparte a partir de las 3 pm hasta las 6 pm. </p> <br>

<p>4.- En caso de reclamo, la única persona que puede reclamar o presentar un reclamo es la que ha realizado la compra. Debido que en todas las compras se solicita DNI, ese número queda asignado como persona autorizada. En algunos casos, según sea demostrado con pruebas extraordinarias; puede asignarse un reclamo. De lo contrario, devoluciones o transferencias serán depositados a dicho Nombre y DNI usado al momento de la compra.    </p>
<p>Quedamos atentos a cualquier cambio que se realice en nuestro proveedor de pagos y POS Culqi; ya que un cambio en sus políticas afecta las nuestras. Por tal motivo, nuestras políticas varían en relación a las Culqui y cualquier cambio, es sin previo aviso. </p>



                    </div>

		        </div>
		    </div>
		</section>

        <section class="counter-up-area">
            <div class="row no-gutters">
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-6">
                    <div class="single-count-box">
                        <div class="counter-img">
                            <img src="img/icon/6.png" alt="">
                        </div>
                        <div class="counter-content">
                            <div class="counter-number">
                                <h2><span class="counter">334</span></h2>
                            </div>
                            <div class="counter-title">
                                <h5>Clientes</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Single Count Box End-->
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-6">
                    <div class="single-count-box">
                        <div class="counter-img">
                            <img src="img/icon/7.png" alt="">
                        </div>
                        <div class="counter-content">
                            <div class="counter-number">
                                <h2><span class="counter">145</span></h2>
                            </div>
                            <div class="counter-title">
                                <h5>Moda</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Single Count Box End-->
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-6">
                    <div class="single-count-box">
                        <div class="counter-img">
                            <img src="img/icon/8.png" alt="">
                        </div>
                        <div class="counter-content">
                            <div class="counter-number">
                                <h2><span class="counter">434</span></h2>
                            </div>
                            <div class="counter-title">
                                <h5>Tiempo</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Single Count Box End-->
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-6">
                    <div class="single-count-box">
                        <div class="counter-img">
                            <img src="img/icon/9.png" alt="">
                        </div>
                        <div class="counter-content">
                            <div class="counter-number">
                                <h2><span class="counter">34366</span></h2>
                            </div>
                            <div class="counter-title">
                                <h5>Ventas</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Single Count Box End-->
            </div>
		</section>

        <!--Footer Area Start-->
        @include('layouts.footer')
        <!--Footer Area End-->
    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-migrate-3.3.2.min.js') }}"></script>
    <!--Popper-->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!--Bootstrap-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--Imagesloaded-->
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!--Isotope-->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!--Countdown-->
    <script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!--ScrollUp-->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!--Chosen js-->
    <script src="{{ asset('js/chosen.jquery.js') }}"></script>
    <!--Meanmenu js-->
    <script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
    <!--Instafeed-->
    <script src="{{ asset('js/instafeed.min.js') }}"></script>
    <!--EasyZoom-->
    <script src="{{ asset('js/easyzoom.min.js') }}"></script>
    <!--Fancybox-->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!--Nivo Slider-->
    <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
    <!--Waypoints-->
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <!--Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!--Slick-->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!--Wow-->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!--Plugins-->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!--Main Js-->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('jquery.flexslider.js') }}"></script>
</body>

</html>
