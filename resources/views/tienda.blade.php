<!doctype html>
<html class="no-js" lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tokecitos.com - Tienda</title>
    <meta name="description" content="Tienda Online - Venta en línea. Outfit de mujeres. Encuentra tu outfit en nuestra tienda">
    <meta name="keywords" content="outfit mujeres, tienda online, outfit damas, outfit para mujeres, vestidos para fiestas"/>
    <meta name="author" content="Tokecitos.com" />
    <meta name="copyright" content="Tokecitos.com" />
    <meta property="og:site_name" content="Tokecitos.com" />
    <meta name="robots" content="index"/>
    <meta name="robots" content="follow"/>
    <meta http-equiv="expires" content="43200"/>
    <meta property=”og:image” content=”https://tokecitos.com/img/offer/verano2023.jpg” />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.rels')

</head>

<body>
    <!--[if lt IE 8]>
 <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->
        <!--Product List Grid View Area Start-->
        <div class="product-list-grid-view-area">
            <div class="container">
                <div class="row">
                    <!--Shop Product Area Start-->
                    <div class="col-lg-9 order-lg-2 order-1">
                        <div class="shop-desc-container">
                            <div class="row">
                                <!--Shop Product Image Start-->
                               
                                @include('layouts.portada')
                                <!--Shop Product Image Start-->
                            </div>
                        </div>
                        <!--Shop Tab Menu Start-->
                        <div class="shop-tab-menu">
                            <div class="row">
                                <!--List & Grid View Menu Start-->
                                <div class="col-lg-5 col-md-5 col-xl-6 col-12">
                                    <div class="shop-tab">
                                        <ul class="nav">
                                            <li><a class="active" data-toggle="tab" href="#grid-view"><i
                                                        class="ion-android-apps"></i></a></li>

                                        </ul>
                                    </div>
                                </div>
                                <!--List & Grid View Menu End-->
                                <!-- View Mode Start-->

                                <!-- View Mode End-->
                            </div>
                        </div>
                        <!--Shop Tab Menu End-->
                        <!--Shop Product Area Start-->
                        <div class="shop-product-area">
                            <div class="tab-content">
                                <!--Grid View Start-->
                                <div id="grid-view" class="tab-pane fade show active">
                                    <div class="row product-container">
                                        @foreach ($productos as $producto)
                                            <!--Single Product Start-->
                                            <div class="col-lg-3 col-md-3 item-col2">
                                                <div class="single-product">
                                                    <div class="product-img">
                                                        <a href="/outfit/{{ $producto->uuid }}">

                                                            <img class="first-img" src="{{ route('welcome') }}/uploads/productimage/{{$producto->imagenes->imagen}}" alt="{{$producto->titulo}}">
                                                        </a>

                                                    </div>
                                                    <!-- <div class="product-content">
                                                        <h2 style="padding: 0 5px;"><a href="/outfit/{{ $producto->uuid }}">
                                                            {{ Str::limit($producto->titulo, 60) }} 
                                                        </a></h2>

                                                        <div class="product-price" style="background-color:#F2786F;">
                                                            <span class="new-price">S/ {{$producto->precio}}</span>
                                                            <a class="button add-btn" style="padding-top:7px;margin-top:5px;" href="/outfit/{{ $producto->uuid }}">ver outfit</a>

                                                            <a class="button add-btn" href="#" data-toggle="tooltip" title="Add to Cart">add to cart</a>
                                                        </div>
                                                    </div> -->

                                                    <div class="product-content">
                                                        <h2><a href="/outfit/{{ $producto->uuid }}">{{ Str::limit($producto->titulo, 60) }} </a></h2>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-o"></i>
                                                        </div>
                                                        <div class="product-price">
                                                            <span class="new-price">S/ {{$producto->precio}}</span>
                                                           <!--  <span class="old-price">-%10</span> -->
                                                            <a class="pt-1 button add-btn" href="/outfit/{{ $producto->uuid }}" data-toggle="tooltip" title="Agregar">al carrito</a>
                                                        </div> 
                                                    </div>

                                                </div>
                                            </div>
                                            <!--Single Product End-->
                                        @endforeach

                                    </div>

                                </div>
                                <!--Grid View End-->

                            </div>
                        </div>
                        <!--Shop Product Area End-->
                        <!--Pagination Start-->
                        <div class="pagination pb-10">
                            {{$productos->links('vendor.pagination.personalizada')}}
                        </div>
                        <!--Pagination End-->
                    </div>
                    <!--Shop Product Area End-->
                    <!--Left Sidebar Start-->
                    <div class="col-lg-3 order-lg-1 order-2">
                        <!--Widget Price Slider Start-->
                        <div class="widget widget-price-slider"  style="margin-bottom: 20px;">
                            <h3 class="widget-title">Filtro por Precio</h3>
                            <div class="widget-content">
                                <div class="price-filter">
                                    <form action="{{route('filtrar')}}" method="get">
                                        @csrf
                                        <div id="slider-range"></div>
                                        <span>Precio:<input id="amount" name="monto" class="amount" readonly=""
                                                type="text"></span>
                                        <input class="price-button" value="Filtrar" type="submit">
                                    </form>
                                </div>
                            </div>
                        </div>

                        

                        @include('layouts.publicidad')
                        

                         

                    </div>


                     



                </div>
            </div>
        </div>
        <!--Product List Grid View Area End-->

        <!--Footer Area Start-->
        @include('layouts.footer')
        <!--Footer Area End-->

    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-migrate-3.3.2.min.js') }}"></script>
    <!--Popper-->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!--Bootstrap-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--Imagesloaded-->
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!--Isotope-->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!--Countdown-->
    <script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!--ScrollUp-->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!--Chosen js-->
    <script src="{{ asset('js/chosen.jquery.js') }}"></script>
    <!--Meanmenu js-->
    <script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
    <!--Instafeed-->
    <script src="{{ asset('js/instafeed.min.js') }}"></script>
    <!--EasyZoom-->
    <script src="{{ asset('js/easyzoom.min.js') }}"></script>
    <!--Fancybox-->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!--Nivo Slider-->
    <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
    <!--Waypoints-->
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <!--Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!--Slick-->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!--Wow-->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!--Plugins-->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!--Main Js-->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('jquery.flexslider.js') }}"></script>
</body>

</html>
