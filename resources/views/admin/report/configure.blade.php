@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Configurar Reporte
        </div>
        <div class="panel-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-20">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong> {{ $message }} </strong>
                </div>
            @endif
            <div class="row">
                <div class="col-md-5">
                    <label for="">Inicio</label>
                    <input type="date" id="inicio" class="form-control">
                </div>
                <div class="col-md-5">
                    <label for="">Fin</label>
                    <input type="date" id="fin" class="form-control">
                </div>

                <div class="col-md-2 text-whiet">
                    <label for="" class="text-white"></label>
                    <button class="btn btn-info pt-5 form-control" onclick="reporter()">Filtrar</button>
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            Reporte
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card text-center">
                        <div class="card-header">
                          Total Ventas
                        </div>
                        <div class="card-body text-numeros total_ventas">
                        </div>
                      </div>
                </div>

                <div class="col-md-6">
                    <div class="card text-center">
                        <div class="card-header">
                          Total Pedidos
                        </div>
                        <div class="card-body text-numeros total_pedidos">
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            Listado de Ventas
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="table-responsive col-md-12">
                    <table class="table hover" style="width:100%" id="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Cliente</th>
                                <th>Monto</th>
                                <th>Tipo de Pago</th>
                            </tr>
                        </thead>
                        <tbody class="data_detalle">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <style>
        .text-numeros{
            font-size: 5rem;
            font-weight: 600
        }
    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.2.2/axios.min.js"
        integrity="sha512-QTnb9BQkG4fBYIt9JGvYmxPpd6TBeKp6lsUrtiVQsrJ9sb33Bn9s0wMQO9qVBFbPX3xHRAsBHvXlcsrnJjExjg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        function reporter() {
            formData = {
                inicio: $("#inicio").val(),
                final: $("#fin").val()
            }

            axios.post('/reporte/getReporte/general', formData).then((response) => {
                $(".total_pedidos").html(response.data.total_pedidos)
                $(".total_ventas").html('S/. '+response.data.total_monto)
                $(".data_detalle").html(response.data)
                const dataDetalle = response.data.pedidos
                dataDetalle.forEach(function(detalle, index) {
                    let columna = "<tr>"
                        columna += "<td>"+detalle.id+"</td>"
                        columna += "<td>"+detalle.cliente.name+"</td>"
                        columna += "<td>S/ "+detalle.total+"</td>"
                        if(detalle.type_transaction == 1){
                            columna += "<td>Tarjeta</td>"
                        }
                        if(detalle.type_transaction == 2){
                            columna += "<td>Yape/Plin/Transferencia</td>"
                        }
                        columna += "</tr>"
                    $("#table>tbody").append(columna);
                    console.log(detalle)
                })
            })


        }
    </script>
@endsection
