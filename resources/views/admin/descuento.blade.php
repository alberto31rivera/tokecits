@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
        Porcentaje Descuento Global
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block mt-20">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong> {{ $message }} </strong>
                                </div>
                            @endif
            <div class="col-md-6">
                <h4>El porcentaje se produce si hay 2 o + productos en pedido</h4>
                <form action="{{route('storeDescuento')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">Descuento:</label>
                        <input type="text" class="form-control" id="descuento" value="{{$descuento}}" name="descuento">
                        <input type="hidden" name="idTalla" id="idTalla" value="0">
                    </div>

                    <button type="submit" class="btn btn-success form-control"> Guardar </button>
                </form>
            </div>
            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>
@endsection
