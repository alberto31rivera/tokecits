@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Listado de Pedidos

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-20">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong> {{ $message }} </strong>
                </div>
            @endif

            <div class="table-responsive col-md-12 ">
                <table class="table table-striped table-bordered table-hover mt-3" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Fecha</th>
                            <th>Monto</th>
                            <th>Cliente</th>
                            <th>Correo</th>
                            <th>Direccion</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pedidos as $pedido)
                            <tr class="odd gradeX">
                                <td>{{ $pedido->id }}</td>
                                <td>{{ $pedido->created_at }}</td>
                                <td>S/ {{ $pedido->total }}</td>
                                <td> {{ $pedido->cliente->name }}</td>
                                <td> {{ $pedido->cliente->email }}</td>
                                <td> {{ $pedido->cliente->Direccion }} - {{ $pedido->cliente->Referencia }}</td>
                                <td>
                                    @if ($pedido->estado == 1)
                                        <span class="badge badge-success">Pagado</span>
                                    @elseif($pedido->estado == 2)
                                        <span class="badge badge-warning">En Espera de Validacion</span>
                                    @elseif($pedido->estado == 3)
                                        <span class="badge badge-danger">Rechazado o Denegado</span>
                                    @elseif($pedido->estado == 4)
                                        <span class="badge badge-primary">Despachado</span>
                                    @elseif($pedido->estado == 5)
                                        <span class="badge badge-danger">Dinero Retornado</span>
                                    @endif
                                </td>
                                <td width="30px">
                                    <a href="{{ route('detallePedido', $pedido->id) }}"><i class="fa fa-eye text-primary"
                                            style="font-size: 18px;"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu: "Mostrar  _MENU_  Registros",
                }
            });
        });
    </script>
@endsection
