@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Configuracion de Cupones - Cuando un cliente compra

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-20">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong> {{ $message }} </strong>
                </div>
            @endif


            <form action="{{ route('procesarMontoCupon') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Monto del Cupon</label>
                            <input type="text" name="montoCupon" value="{{ $config->valor }}" class="form-control">
                        </div>

                        <input type="submit" value="Guardar" class="btn btn-success from-control">
                    </div>
                </div>
            </form>

        </div>
        <!-- /.panel-body -->
    </div>
@endsection
