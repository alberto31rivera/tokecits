<html lang="es">

<head>

    @include('layouts.analitycs')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Pago Exitoso. Outfit mujer, vestidos para xv, outfit para damas.">
    <meta name="keywords" content="outfit mujeres, blusas para compromisos, vestidos para xv, outfit damas, outfit para mujeres, vestidos para fiestas"/>
    <meta name="author" content="Tokecitos.com" />
    <meta name="copyright" content="Tokecitos.com" />
    <meta property="og:site_name" content="Tokecitos.com" />
    <meta name="robots" content="noindex"/>
    <meta name="robots" content="nofollow"/>
    <meta http-equiv="expires" content="43200"/>

    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Pago Exitoso</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/checkout/">

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/checkout/form-validation.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body class="bg-light">

    <div class="container">
        <div class="row">
            <div class="col-md-4 mt-5">
                <img class="d-block mb-4" src="https://tokecitos.com/public/logo-dia.png" alt="" width="250"
                    height="72">
            </div>
            <div class="col-md-4 offset-md-4 mt-5">
                {{--  <a href="/" class="btn btn-info btn-lg btn-block" type="submit">Regresar a la Tienda</a>  --}}
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="jumbotron">
                    <div class="container text-center">
                        <i class="fa-solid fa-clipboard-check text-info" style="font-size: 12rem"></i>
                        <p class="lead" style="margin-top: 1rem">Compra Procesada y Pagada con exito!</p>

                        <a href="/cuenta" class="btn btn-info"> Ir a Mi Cuenta </a>
                    </div>
                </div>
            </div>

        </div>


        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">&copy; 2023 Tokecitos</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Privacidad</a></li>
                <li class="list-inline-item"><a href="#">Terminos</a></li>
                <li class="list-inline-item"><a href="#">Soporte</a></li>
            </ul>
        </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script>
        window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>

</body>

</html>
