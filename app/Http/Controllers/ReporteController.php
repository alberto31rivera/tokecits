<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use Illuminate\Http\Request;

class ReporteController extends Controller
{
    public function ventas_globales()
    {
        return view('admin.report.configure');
    }

    public function get_global(Request $request)
    {
        $pedidos = Pedido::where('estado', [1, 4])->whereBetween('fecha_pago', [$request->inicio, $request->final])->with('cliente')->get();
        $monto_total = $pedidos->sum('total');
        $pedido_total = $pedidos->count();

        return response()->json([
            'total_pedidos' => $pedido_total,
            'total_monto' => $monto_total,
            'pedidos' => $pedidos
        ]);
    }
}
