<?php

namespace App\Http\Controllers;

use App\Models\Configuracion;
use App\Models\Cupon;
use App\Models\DireccionClientes;
use App\Models\Pedido;
use App\Models\User;
use Illuminate\Http\Request;

class PagoController extends Controller
{
    public function pasarela_proceso(Request $request)
    {
        $pedido = Pedido::find($request->idPedido);
        $monto = $request->monto * 100;
        $user = User::whereEmail($request->correo)->first();

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.culqi.com/v2/charges');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"amount\": \"$monto\",\n  \"currency_code\": \"PEN\",\n  \"email\": \"$request->correo\",\n  \"source_id\":\"$request->tokenCulqi\"\n}");

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Bearer sk_test_6jeZmv0L9qr2QpTE';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result, true);

        if (isset($result['outcome']['type']) && $result['outcome']['type'] == "venta_exitosa") {
            $pedido->type_transaction = 1;
            $pedido->tarjeta = $result['source']['card_number'];
            $pedido->marca_tarjeta = $result['source']['iin']['card_brand'];
            $pedido->descripcion_tarjeta = $result['outcome']['merchant_message'];
            $pedido->cargo_id = $result['id'];
            $pedido->estado = 1;
            $pedido->fecha_pago = date('Y-m-d');
            $pedido->id_cliente = $user->id;
            $pedido->save();

            $direccion = DireccionClientes::find($pedido->dir_envio);

            $this->actualizarOrCreateCupon($direccion->dni);

            if($pedido->cupon != null)
            {
                $this->eliminarCuponUnicoUso($pedido->cupon);
            }


            return 200;
        }

        else
        {
            return $result;
        }


    }


    public function pago_exitoso()
    {
        return view('exito_pago');
    }

    public function actualizarOrCreateCupon($codigoCupon)
    {
        $config = Configuracion::where('configuracion', 'monto_cupon_compra')->first();

        if($config->valor == 0){
            $cupon = Cupon::whereCodigo($codigoCupon)->first();
            $cupon->delete();
        }
        else{
            $cupon = Cupon::updateOrCreate([
                'codigo' => $codigoCupon
            ],
            [
                'aplica' => $config->valor,
                'unico_uso' => 1,
                'tipo' => 2,
                'estado' => 1
            ]);
        }

    }

    public function eliminarCuponUnicoUso($cuponId)
    {
        $cupon = Cupon::find($cuponId);
        if($cupon->unico_uso == 1){
            $cupon->estado = 2;
            $cupon->save();
        }
    }
}
