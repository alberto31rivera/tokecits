<?php

namespace App\Http\Controllers;

use App\Models\Configuracion;
use App\Models\Cupon;
use Illuminate\Http\Request;

class CuponController extends Controller
{
    public function index()
    {
        return view('admin.cupones', [
            'cupones' => Cupon::all()
        ]);
    }


    public function store(Request $request)
    {
        Cupon::updateOrCreate(
            [
                'codigo' => $request->codigo
            ],
            [
                'tipo' => $request->tipo,
                'aplica' => $request->cantidad
            ]
        );

        return back()->with('success', 'Cupon Creado / Actualizado con exito');
    }

    public function cuponesCompra(Request $request)
    {
        $config = Configuracion::where('configuracion', 'monto_cupon_compra')->first();
        return view('admin.cuponesConfiguracion', ['config' => $config]);
    }

    public function procesarMontoCupon(Request $request)
    {
        $config = Configuracion::where('configuracion', 'monto_cupon_compra')->first();
        $config->valor = $request->montoCupon;
        $config->save();


         return back()->with('success', 'Monto del Cupon Actualizado con exito');
    }
}
