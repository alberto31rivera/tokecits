<?php

namespace App\Http\Controllers;

use App\Models\{User, Pedido, RelacionPedidoProducto};
use Auth;
use Illuminate\Http\Request;
use Session;

class ClienteController extends Controller
{
    public function login_carrito( Request $request )
    {
        $val = User::whereEmail($request->username)
                    ->wherePassword(sha1($request->password))
                    ->count();

        if($val > 0)
        {
            $val = User::whereEmail($request->username)
                ->wherePassword(sha1($request->password))
                ->first();
            Auth::loginUsingId($val->id);
            return back();
        }
        else
        {
            return back()->with('success', 'Tus datos no existen en nuestra db');
        }
    }

    public function loginproceso(Request $request)
    {
        $val = User::whereEmail($request->username)
            ->wherePassword(sha1($request->password))
            ->count();

        if ($val > 0) {
            $val = User::whereEmail($request->username)
                ->wherePassword(sha1($request->password))
                ->first();
            Auth::loginUsingId($val->id);
            return redirect()->route('micuenta');
        } else {
            return back()->with('success', 'Tus datos no existen en nuestra db');
        }
    }

    public function login_post(Request $request)
    {
        $val = User::whereEmail($request->username)
            ->wherePassword(sha1($request->password))
            ->count();

        if ($val > 0) {
            $val = User::whereEmail($request->username)
                ->wherePassword(sha1($request->password))
                ->first();
            Auth::loginUsingId($val->id);
            return redirect()->route('cuenta');
        } else {
            return back()->with('success', 'Tus datos no existen en nuestra db');
        }
    }

    public function registro( Request $request )
    {
        $new = new User();
        $new->email = $request->username;
        $new->password = sha1($request->password);
        $new->name = "Usuario";
        $new->provider = "laravel";
        $new->tipoUsuario = 2;
        $new->save();

        Auth::loginUsingId($new->id);
        return back();
    }

    public function login()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $pedido = Pedido::where('ip_pedido', $result)
            ->where('estado', 0)
            ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
            ->first();
        if (isset($pedido->id)) {
            $cantidad = RelacionPedidoProducto::where('id_pedido', $pedido->id)->sum('cantidad');
        } else {
            $cantidad = 0;
        }

        return view('login', compact('pedido', 'cantidad'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('welcome');
    }
}
