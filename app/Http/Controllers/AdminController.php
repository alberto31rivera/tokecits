<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\RelacionPedidoProducto;
use App\Models\PorcentajeDescuento;
use App\Models\{Cupon, Distrito, Deprtamento, Provincia, User};
use Illuminate\Http\Request;
use Auth;

class AdminController extends Controller
{
    public function login()
    {
        if(isset(Auth::user()->tipoUsuario) && Auth::user()->tipoUsuario == 1)
        {
            return redirect()->route('indexAdmin');
        }
        else
        {
            return view('admin.login');
        }

        return view('admin.login');
    }

    public function loguearse(Request $request)
    {
        $val = User::whereEmail($request->email)
            ->wherePassword(sha1($request->password))
            ->count();

        if ($val > 0) {
            $val = User::whereEmail($request->email)
                ->wherePassword(sha1($request->password))
                ->first();
            Auth::loginUsingId($val->id);
            return redirect()->route('indexAdmin');
        } else {
            return back()->with('success', 'Tus datos no existen en nuestra db, o no tienes los permisos necesarios');
        }
    }

    public function index()
    {
        if(isset(Auth::user()->name))
        {
            return view('admin.home');
        }
        else{
            return redirect()->route('loginAdmin');
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('welcome');
    }
}
