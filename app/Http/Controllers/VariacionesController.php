<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVariacionesRequest;
use App\Http\Requests\UpdateVariacionesRequest;
use App\Models\Variaciones;

class VariacionesController extends Controller
{
    public function stock($idVariante)
    {
        $variante = Variaciones::find($idVariante);

        return $variante;
    }
}
