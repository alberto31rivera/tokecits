<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variaciones extends Model
{
    use HasFactory;
    protected $fillable = [
        'idProducto',
        'idTalla',
        'idColor',
        'idMaterial',
        'stock'
    ];

    public function color_detalle()
    {
        return $this->hasOne(Clores::class, 'id', 'idColor');
    }

    public function talla_detalle()
    {
        return $this->hasOne(Talla::class, 'id', 'idTalla');
    }

    public function material_detalle()
    {
        return $this->hasOne(Materiales::class, 'id', 'idMaterial');
    }
}
