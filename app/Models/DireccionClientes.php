<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DireccionClientes extends Model
{
    use HasFactory;

    protected $table = "direccion_clientes";

    protected $fillable = [
        'id_cliente',
        'nombre',
        'apellido',
        'telefono',
        'dni',
        'ruc',
        'calle',
        'referencia',
        'departamento',
        'provincia',
        'distrito',
        'pais',
        'codigo_postal',
        'defecto',
    ];

}
